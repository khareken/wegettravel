//package com.example.weget.retrofit
//
//import com.example.weget.models.SearchData
//import retrofit2.Call
//import retrofit2.http.Field
//import retrofit2.http.FormUrlEncoded
//import retrofit2.http.POST
//
//
//interface ApiSearchInhterface{
//
//
//    //Здесь пост запросы для поиска
//
//    @FormUrlEncoded
//    @POST("search")
//    fun search(@Field("query") text: String,
//               @Field("check_in") checkIn: String,
//               @Field("check_out") checkOut: String,
//               @Field("guest_adult") guestsA: Int,
//               @Field("guest_children") guestsCh: Int,
//               @Field("duration") duration: Int) : Call<List<SearchData>>
//}