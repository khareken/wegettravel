package com.example.weget.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MarginDecorationItem(
    private val bigMargin: Int,
    private val veryBigMargin: Int,
    private val type: String
) :
    RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {

            if (type == "l_v") {
                if (parent.getChildAdapterPosition(view) == 0) {
                    top = bigMargin
                }

                bottom = bigMargin



//                right = bigMargin
//                left  = bigMargin
            } else if (type == "l_h") {

                bottom = bigMargin
                right = bigMargin
                top = bigMargin

                if (parent.getChildAdapterPosition(view) == 0) {
                    left = veryBigMargin}
                    if (parent.getChildAdapterPosition(view) == parent.adapter?.itemCount?.minus(
                            1
                        )
                    ) {
                        right = veryBigMargin
                    }

            }


        }
    }

}