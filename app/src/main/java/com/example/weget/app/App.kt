package com.example.weget.app

import android.app.Application
import com.example.weget.helper.SharedPreferenceHelper

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        SharedPreferenceHelper.init(this)

    }
}