//package com.example.weget.room
//
//import android.content.Context
//import androidx.room.Database
//import androidx.room.Room
//import androidx.room.RoomDatabase
//import com.example.weget.models.HotelFullData
//
//@Database(entities = [HotelFullData::class],version = 1)
//abstract class AppDatabase:RoomDatabase(){
//        abstract fun getInputData(): InputDao
//
//    companion object{
//        var database: AppDatabase?=null
//        fun init(context: Context){
//            database = Room.databaseBuilder(
//                context.applicationContext, AppDatabase::
//                        class.java,"hotel-database"
//            )
//                .allowMainThreadQueries()
//                .build()
//        }
//    }
//}
