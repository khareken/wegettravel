//package com.example.weget.room
//
//import androidx.lifecycle.LiveData
//import androidx.room.Dao
//import androidx.room.Insert
//import androidx.room.OnConflictStrategy
//import androidx.room.Query
//import com.example.weget.models.HotelFullData
//
//@Dao
//interface InputDao {
//    @Query("SELECT * FROM HotelFullData")
//    fun get(): LiveData<List<HotelFullData>>
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun save(inputData: HotelFullData)
//
//    @Query("SELECT * FROM HotelFullData")
//    fun getHotelOffline():List<HotelFullData>
//
//    @Query("SELECT * FROM HotelFullData where id = :id")
//    fun getHotelOfflineById(id: Int): HotelFullData
//}