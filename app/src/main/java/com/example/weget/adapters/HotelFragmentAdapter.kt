package com.example.weget.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.weget.R
import kotlinx.android.synthetic.main.item_of_list_hotels.view.*


class HotelFragmentAdapter: RecyclerView.Adapter<HotelFragmentAdapter.HotelFragmentViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelFragmentViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).
                inflate(R.layout.item_of_list_hotels, parent, false)
        return HotelFragmentViewHolder(itemView)
    }

    override fun getItemCount(): Int = 20


    override fun onBindViewHolder(holder: HotelFragmentViewHolder, position: Int) {
            holder.populateModel()
    }
    inner class HotelFragmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun populateModel(){

//            Glide.with(itemView)
//                .load(R.drawable.samarqand)
//                .apply(RequestOptions().override(100,100))
//                .into(itemView.hotel_fragment_item_imageView)
        }
    }
}

