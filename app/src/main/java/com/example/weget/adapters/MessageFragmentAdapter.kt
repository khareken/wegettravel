package com.example.weget.fragments.hotel.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weget.R

class MessageFragmentAdapter : RecyclerView.Adapter<MessageFragmentAdapter.MessageFragmentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageFragmentViewHolder =
        MessageFragmentViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_of_list_messenger, parent, false)
        )

    override fun getItemCount(): Int = 20


    override fun onBindViewHolder(holder: MessageFragmentViewHolder, position: Int) {
            holder.populateModel()
    }
    inner class MessageFragmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun populateModel(){
        }
    }
}

