package com.example.weget.adapters

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.weget.fragments.BaseFragment

@Suppress("DEPRECATION")
class PagerAdapter(fm: FragmentManager, var behavior: List<BaseFragment> ) :
FragmentPagerAdapter(fm){
    override fun getItem(position: Int)= behavior[position]

    override fun getCount() = behavior.size

}