package com.example.weget.adapters

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.weget.R
import kotlinx.android.synthetic.main.item_of_list_hotels.view.*
import kotlinx.android.synthetic.main.item_of_list_regions.view.*
import kotlinx.android.synthetic.main.item_popular_properties.view.*


class BaseAdapter(var list:String) : RecyclerView.Adapter<BaseAdapter.HomeFragmentViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeFragmentViewHolder {
        when (list) {
            "region" -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_of_list_regions, parent, false)
                return HomeFragmentViewHolder(
                    itemView
                )
            }
            "category" -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_category_properties, parent, false)
                return HomeFragmentViewHolder(
                    itemView
                )
            }
            "popular" -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_popular_properties, parent, false)
                return HomeFragmentViewHolder(
                    itemView
                )
            }
            "hotel" -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_of_list_hotels, parent, false)
                return HomeFragmentViewHolder(
                    itemView
                )
            }
            else -> {
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_tours, parent, false)
                return HomeFragmentViewHolder(
                    itemView
                )
            }
        }
    }

    override fun getItemCount(): Int = 10

    override fun onBindViewHolder(holder: HomeFragmentViewHolder, position: Int) {
        holder.bindData(list)
    }


    inner class HomeFragmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindData(list: String) {

//            Glide.with(itemView)
//                .load(R.drawable.samarqand)
//                .override(100,500)
//                .into(itemView.regionImage)

            when (list) {
                "popular" -> {
                    itemView.popularItemStar.stepSize = 0.5F
                    itemView.popularItemStar.progress = 9
                }
                "hotel"->{
                    Glide.with(itemView)
                        .load(R.drawable.hotel_2)
                        .apply(RequestOptions.centerCropTransform())
                        .into(itemView.hotel_fragment_item_imageView)
                    val spannable = SpannableString("Rating: 8/10")
                    spannable.setSpan(ForegroundColorSpan(Color.LTGRAY),0,7,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//                    itemView.hotel_fragment_item_ratingBar =
                    itemView.alpha=0.1f
                    itemView.animate().alpha(1f).setDuration(300).start()
                    itemView.hotel_fragment_item_text_rating.text = spannable
                    itemView.hotel_fragment_item_ratingBar.stepSize = 0.5F
                    itemView.hotel_fragment_item_ratingBar.progress = 9
                }
                "region"->{
                    Glide.with(itemView)
                        .load(R.drawable.samarqand)
                        .apply(RequestOptions.centerCropTransform())
                        .into(itemView.regionImage)
                }
            }


        }
    }

}