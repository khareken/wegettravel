package com.example.weget.fragments

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import com.example.weget.R
import com.example.weget.adapters.PagerAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment(R.layout.fragment_main) {
    private lateinit var fragments:ArrayList<BaseFragment>
    lateinit var adapter: PagerAdapter
//    private lateinit var manager: FragmentManager
//    private val mOnNavigationItemSelectedListener =
//        bottomNav.setOnClickMenuListener{
//            when (it.id) {
//                R.id.navigation_home -> {
//                    bottomNav.show(0)
//
//                }
//
//                R.id.navigation_hotel -> {
//                    bottomNav.show(1)
//                }
//                R.id.navigation_account -> {
//                    bottomNav.show(2)
//                }
//                R.id.navigation_message -> {
//                    bottomNav.show(3)
//                }
//                R.id.navigation_bottom_menu -> {
//                    bottomNav.show(4)
//                }
//            }
//
//        }

//
//

    override fun onViewCreate() {
        addFragment()
//        mOnNavigationItemSelectedListener
        adapter = PagerAdapter(activity!!.supportFragmentManager,fragments)
        viewPager.adapter  = adapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                bottomNav.show(position)
            }

        })
            bottomNav.setOnClickMenuListener {
                viewPager.currentItem = it.id
            }
            viewModel.setLastFragment {
                bottomNav.show(it)
                viewPager.currentItem = it
            }
    }
    private fun addFragment(){
        fragments = ArrayList()
        fragments.add(HomeFragment())
        fragments.add(HotelFragment())
        fragments.add(AccountFragment())
        fragments.add(MessageFragment())
        fragments.add(BottomMenuFragment())

        bottomNav.add(MeowBottomNavigation.Model(0,R.drawable.ic_home_black_24dp))
        bottomNav.add(MeowBottomNavigation.Model(1,R.drawable.ic_hotel_24dp))
        bottomNav.add(MeowBottomNavigation.Model(2,R.drawable.ic_account_circle_black_24dp))
        bottomNav.add(MeowBottomNavigation.Model(3,R.drawable.ic_local_post_office_black_24dp))
        bottomNav.add(MeowBottomNavigation.Model(4,R.drawable.ic_more_vert_black_24dp))
        bottomNav.show(0)
    }
}
