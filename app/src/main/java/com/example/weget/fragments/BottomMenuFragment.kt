package com.example.weget.fragments

import android.graphics.Color
import com.example.weget.R
import com.example.weget.helper.SharedPreferenceHelper
import kotlinx.android.synthetic.main.fragment_bottom_menu.*

class BottomMenuFragment : BaseFragment(R.layout.fragment_bottom_menu) {
//   override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//    }

    override fun onViewCreate() {

        if (SharedPreferenceHelper.pref!!.isNightMode()) {
            bottomMenu.setBackgroundColor(Color.MAGENTA)
            switchToNightMode.isChecked = true
        } else {
            bottomMenu.setBackgroundColor(Color.WHITE)
            switchToNightMode.isChecked = false
        }

        switchToNightMode.setOnClickListener {
            if (switchToNightMode.isChecked) {
                SharedPreferenceHelper.pref!!.setNightMode(true)
                bottomMenu.setBackgroundColor(Color.MAGENTA)
            } else {
                SharedPreferenceHelper.pref!!.setNightMode(false)
                bottomMenu.setBackgroundColor(Color.WHITE)
            }

        }
    }
}