package com.example.weget.fragments

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.weget.R
import com.example.weget.fragments.hotel.list.MessageFragmentAdapter
import kotlinx.android.synthetic.main.fragment_message.*

class MessageFragment : BaseFragment(R.layout.fragment_message) {
    private val messageFragmentAdapter = MessageFragmentAdapter()

    override fun onViewCreate() {
        messengerList.adapter = messageFragmentAdapter
        messengerList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        messengerList.addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL))

    }


}