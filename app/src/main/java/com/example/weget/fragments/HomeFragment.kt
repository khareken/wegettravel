package com.example.weget.fragments

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.weget.R
import com.example.weget.adapters.BaseAdapter
import com.example.weget.utils.MarginDecorationItem
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.home_search.*

class HomeFragment : BaseFragment(R.layout.fragment_home) {
    lateinit var type: String


    //    var bottomSheetBehavior = BottomSheetBehavior.from(search_dialog_layout)
    private lateinit var sheetBehavior: BottomSheetBehavior<ConstraintLayout>

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
////        sheetBehavior = BottomSheetBehavior.from(searchBottomSheet)
//
//
//
//
//
//
//    }

    override fun onViewCreate() {
        regionsList.adapter = BaseAdapter("region")
        regionsList.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        regionsList.addItemDecoration(
            MarginDecorationItem(
                resources.getDimension(R.dimen.large_16dp).toInt(),
                resources.getDimension(R.dimen.very_large_24dp).toInt()
            ,"l_h")
        )

        categoryPropertiesList.adapter = BaseAdapter("category")
        categoryPropertiesList.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        categoryPropertiesList.addItemDecoration(
            MarginDecorationItem(
                resources.getDimension(R.dimen.large_16dp).toInt(),
                resources.getDimension(R.dimen.very_large_24dp).toInt()
            ,"l_h")
        )


        popularPropertiesList.adapter = BaseAdapter("popular")
        popularPropertiesList.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        popularPropertiesList.addItemDecoration(
            MarginDecorationItem(
                resources.getDimension(R.dimen.large_16dp).toInt(),
                resources.getDimension(R.dimen.very_large_24dp).toInt()
            ,"l_h")
        )

        toursList.adapter = BaseAdapter("tour")
        toursList.layoutManager = GridLayoutManager(this.context, 2)
        toursList.addItemDecoration(
            MarginDecorationItem(
                resources.getDimension(R.dimen.large_16dp).toInt(),
                resources.getDimension(R.dimen.very_large_24dp).toInt()
                ,"g_2")
        )

        homeSearch.setOnClickListener {
            showBottomSheetDialogFragment()


        }
        Glide.with(this)
            .load(R.drawable.top_image_bg)
            .apply(RequestOptions().centerCrop())
            .apply(RequestOptions().override(0, 200))
            .into(topImage)//biraq mina jerde qate shigarmay atir
        Glide.with(this)
            .load(R.drawable.top_image_radial_grad)
            .apply(RequestOptions().centerCrop())
            .apply(RequestOptions().override(0, 200))
            .into(topImageBackground)//biraq mina jerde qate shigarmay atir
    }


    private fun showBottomSheetDialogFragment() {
        val bottomSheetSearchDialog =
            HomeSearchFragment()
        bottomSheetSearchDialog.show(requireFragmentManager(), bottomSheetSearchDialog.tag)
    }
}