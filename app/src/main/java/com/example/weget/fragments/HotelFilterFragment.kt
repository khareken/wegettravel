package com.example.weget.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.weget.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_hotel.*

class HotelFilterFragment : BottomSheetDialogFragment() {
    private var fragmentView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState:

        Bundle?
    ): View? {
        fragmentView = inflater.inflate(R.layout.bottom_sheet_dialog_search, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(this)

    }

    private fun initView(hotelBottomSheetDialog: HotelFilterFragment) {

            seekBar.setOnRangeSeekBarChangeListener { _, number, number2 ->
            mutableAvgPrice.text = "$$number - $$number2"
        }
        minusBtn.setOnClickListener {
            numberOfAdults.text = (numberOfAdults.text.toString().toInt() - 1).toString()
        }
        plusBtn.setOnClickListener {
            numberOfAdults.text = (numberOfAdults.text.toString().toInt() + 1).toString()
        }

        minusBtnChildren.setOnClickListener {
            numberOfChildren.text = (numberOfAdults.text.toString().toInt() - 1).toString()
        }
        plusBtnChildren.setOnClickListener {
            numberOfChildren.text = (numberOfAdults.text.toString().toInt() + 1).toString()
        }

        btnClose.setOnClickListener {
            hotelBottomSheetDialog.dismiss()
        }
    }
}

