package com.example.weget.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.weget.viewModel.FragmentViewModel

abstract class BaseFragment(@LayoutRes val layoutId: Int) : Fragment() {
     lateinit var viewModel: FragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(activity!!)[FragmentViewModel::class.java]
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreate()
    }

    abstract fun onViewCreate()
    fun <T : BaseFragment> replaceFragment(fragment: Class<T>) {
        viewModel.replaceFragment(fragment)
    }

    fun <T : BaseFragment> startMainFragment(fragment: Class<T>) {
        viewModel.startMainFragment(fragment)
    }

    fun <T : BaseFragment> startMainFragmentAdd(fragment: Class<T>) {
        viewModel.startMainFragmentAdd(fragment)
    }

    fun closeActiveFragment() {
        viewModel.closeActiveFragment()
    }
}