package com.example.weget.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.weget.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_search.*
import java.util.*

class HomeSearchFragment : BottomSheetDialogFragment() {
    private var fragmentView: View? = null
    var c = Calendar.getInstance()
    var year1 = c.get(Calendar.YEAR)
    var month1 = c.get(Calendar.MONTH)
    var day1 = c.get(Calendar.DAY_OF_MONTH)
    var year2 = c.get(Calendar.YEAR)
    var month2 = c.get(Calendar.MONTH)
    var day2 = c.get(Calendar.DAY_OF_MONTH)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState:Bundle?
    ): View? {
        fragmentView = inflater.inflate(R.layout.bottom_sheet_dialog_search, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(this)

    }

    private fun initViews(homeSearchFragment: HomeSearchFragment) {
        homeSearchFragment.btnClose.setOnClickListener {
            homeSearchFragment.dismiss()
        }
        homeSearchFragment.check_in_text_and_date_area.setOnClickListener{
            val dpd  = DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth -> homeSearchFragment.check_in_date.setText(dayOfMonth.toString()+"/"+(month+1)+"/"+year) },year1,month1,day1)
            dpd.datePicker.minDate=System.currentTimeMillis()
            dpd.show()
        }
        homeSearchFragment.check_out_text_and_date_area.setOnClickListener{
            val dpd  = DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth -> homeSearchFragment.check_out_date.setText(dayOfMonth.toString()+"/"+(month+1)+"/"+year) },year2,month2,day2)
            dpd.datePicker.minDate=System.currentTimeMillis() // изменим логику
            dpd.show()
        }
    }

}

