package com.example.weget.fragments

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.weget.R
import com.example.weget.adapters.BaseAdapter
import com.example.weget.adapters.HotelFragmentAdapter
import com.example.weget.models.HotelFullData
import com.example.weget.utils.MarginDecorationItem
import kotlinx.android.synthetic.main.fragment_hotel.*
import kotlinx.android.synthetic.main.item_of_list_hotels.*
import kotlinx.android.synthetic.main.top_rating_and_filter_texts_with_icons.*

class HotelFragment : BaseFragment(R.layout.fragment_hotel) {
   lateinit var data:ArrayList<HotelFullData>


    override fun onViewCreate() {
        hotelsList.adapter = BaseAdapter("hotel")
        hotelsList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        hotelsList.addItemDecoration(
            MarginDecorationItem(
                resources.getDimension(R.dimen.large_16dp).toInt(),
                resources.getDimension(R.dimen.very_large_24dp).toInt()
                ,"l_v")
        )





    }
    private fun showHotelBottomSheet(){
        val hotelBottomSheetDialog =
            HotelFilterFragment()
        hotelBottomSheetDialog.show(requireFragmentManager(),hotelBottomSheetDialog.tag)
    }

}