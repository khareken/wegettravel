package com.example.weget.viewModel

import androidx.annotation.LayoutRes
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weget.R
import com.example.weget.fragments.BaseFragment

class FragmentViewModel : ViewModel() {
    private val fragments = HashMap<String, BaseFragment>()
    lateinit var manager: FragmentManager
    val inputTextLiveData = MutableLiveData<String>()
    val activeFragment = MutableLiveData<String>()
    var menuListener = MutableLiveData<Int>()
    var isOpenNav = false
    var sanatoriumaId=0
//    val repository = AppRepository()
    private var listener: ((String) -> Unit)? = null
    var listenerLastFragment: ((Int) -> Unit)? = null
    var listenerMenu: ((Boolean) -> Unit)? = null
    @LayoutRes
    var layoutId: Int = 0

    fun <T : BaseFragment> replaceFragment(fragment: Class<T>) {
        manager.beginTransaction().addToBackStack(fragment.toString())
            .replace(layoutId, getFragment(fragment)).commit()
    }

    fun <T : BaseFragment> startMainFragment(fragment: Class<T>) {
        manager.beginTransaction().replace(layoutId, getFragment(fragment)).commit()
    }
    fun <T : BaseFragment> startMainFragmentAdd(fragment: Class<T>) {
        manager.beginTransaction().setCustomAnimations(
            R.anim.slide_in_right,
            R.anim.slide_out_left,
            R.anim.slide_in_right,
            R.anim.slide_out_left
        ).addToBackStack(getFragment(fragment).toString())
            .add(layoutId, getFragment(fragment)).commit()
    }

    private fun <T : BaseFragment> getFragment(fragment: Class<T>): BaseFragment {
        listener?.invoke(fragment.name)
        if (!fragments.contains(fragment.name)) {
            fragments[fragment.name] = fragment.newInstance()
        }
        return fragments[fragment.name]!!
    }

    fun closeActiveFragment() {
        if (fragments.size > 0) manager.popBackStack()
    }
    fun setFragmentChangeListener(f: (String) -> Unit) {
        listener = f
    }
    fun setLastFragment(f: (Int) -> Unit) {
        listenerLastFragment = f
    }
}