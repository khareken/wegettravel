package com.example.weget

import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity



class SplashActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_splash)
            object :CountDownTimer(3000,1000){
                override fun onFinish() {
                }

                override fun onTick(millisUntilFinished: Long) {
                        checking()
                }

            }.start()
    }

    fun checking(){

    }
}
