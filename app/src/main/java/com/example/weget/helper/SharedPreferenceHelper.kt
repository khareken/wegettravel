package com.example.weget.helper

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor

class SharedPreferenceHelper(context: Context) {
    private lateinit var preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(
            "MySettings",
            Context.MODE_PRIVATE
        )

    }

    companion object {
        const val IS_NIGHT_MODE = "isNightMode"
         var pref: SharedPreferenceHelper?=null
        fun init(context: Context): SharedPreferenceHelper {
            if (pref == null) {
                pref = SharedPreferenceHelper(context)
            }
            return pref!!
        }
    }


    fun isNightMode(): Boolean {
        return preferences.getBoolean(IS_NIGHT_MODE, false)
    }

    fun setNightMode(nightMode: Boolean) {
        preferences.edit().putBoolean(IS_NIGHT_MODE, nightMode).apply()
    }
}