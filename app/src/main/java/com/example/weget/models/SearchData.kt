//package com.example.weget.models
//
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import com.google.gson.annotations.SerializedName
//
//@Entity
//data class SearchData (
//    @PrimaryKey
//    var id: Int,
//    @SerializedName("city_id")
//    var cityId: Int,
//    @SerializedName("owner_id")
//    var ownerId: Int,
//    @SerializedName("hotel_type_id")
//    var hotelTypeId: Int,
//    @SerializedName("hotel_name")
//    var hotelName: String,
//    var rating: Int,
//    @SerializedName("hotel_region")
//    var hotelRegion: Int,
//    @SerializedName("postal_code")
//    var postalCode: Int,
//    @SerializedName("hotel_address")
//    var hotelAddress: String,
//    @SerializedName("address_map")
//    var addressMap: String,
//    @SerializedName("description_en")
//    var descriptionEn: String,
//    @SerializedName("description_ru")
//    var descriptionRu: String,
//    @SerializedName("description_uz")
//    var descriptionUz: String,
//    var status: Int,
//    var done: Int,
//    @SerializedName("created_at")
//    var createdAt: String,
//    @SerializedName("updated_at")
//    var updatedAt: String,
//    var center: Int,
//    var gallery:String
//    )