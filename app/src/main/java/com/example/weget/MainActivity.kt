package com.example.weget

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.weget.fragments.MainFragment
import com.example.weget.viewModel.FragmentViewModel

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: FragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //pushni quyidagicha qilasiz kuropsizmi ?ha
//        val navView: BottomNavigationView = findViewById(R.id.nav_view)
//
//        val navController = findNavController(R.id.nav_host_fragment)
//        // Passing each menu ID as a set of Ids because each
//        // menu should be considered as top level destinations.
//
//////        val appBarConfiguration = AppBarConfiguration(
//////            setOf(
//////                R.id.navigation_search, R.id.navigation_favorites, R.id.navigation_inbox
//////            )
//////        )
//////        setupActionBarWithNavController(navController, appBarConfiguration)
////
//        navView.setupWithNavController(navController)
        viewModel = ViewModelProviders.of(this)[FragmentViewModel::class.java]
        viewModel.apply {
            layoutId = R.id.container
            manager = supportFragmentManager
            if (savedInstanceState == null)
                startMainFragment(MainFragment::class.java)
        }

    }
}
