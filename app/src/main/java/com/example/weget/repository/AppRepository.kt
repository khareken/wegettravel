//package com.example.weget.repository
//
//import android.widget.Toast
//import androidx.appcompat.app.AppCompatActivity
//import androidx.lifecycle.LiveData
//import com.example.weget.models.HotelFullData
//import com.example.weget.retrofit.ApiClient
//import com.example.weget.retrofit.ApiInterface
//import com.example.weget.retrofit.ApiSearchInhterface
//import com.example.weget.room.AppDatabase
//import retrofit2.Call
//import retrofit2.Callback
//import retrofit2.Response
//
//class AppRepository{
//    private var hotelListLiveData: LiveData<List<HotelFullData>>? = null
//
//    private var apiInterface: ApiInterface? = ApiClient.builder().create(ApiInterface::class.java)
//    private var apiSerachInterface: ApiSearchInhterface? = ApiClient.builder().create(ApiSearchInhterface::class.java) // Запросы для поиска
//
//
//    fun getSanatoriumList(): LiveData<List<HotelFullData>> {
//        if (hotelListLiveData == null) {
//            hotelListLiveData = AppDatabase.database?.getInputData()?.get()
//        }
//        apiInterface!!.getHotels().enqueue(object : Callback<List<HotelFullData>> {
//            override fun onFailure(call: Call<List<HotelFullData>>, t: Throwable) {
//            }
//
//            override fun onResponse(
//                call: Call<List<HotelFullData>>,
//                response: Response<List<HotelFullData>>
//            ) {
//                if (response.isSuccessful) {
//                    if (response.body() != null) {
//                        addSanatories(response.body()!!)
//                    }
//                }
//            }
//        })
//        return hotelListLiveData!!
//    }
//    fun addSanatories(hotels: List<HotelFullData>) {
//        hotels.forEach { hotels -> AppDatabase.database?.getInputData()?.save(hotels) }
//        hotelListLiveData = AppDatabase.database?.getInputData()?.get()
//    }
//
//    fun getHotelOffline(id:Int):HotelFullData{
//         return AppDatabase.database?.getInputData()?.getHotelOfflineById(id)!!
//    }
//}
